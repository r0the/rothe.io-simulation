# Modell

Um den Fall eines Objekts zu beschreiben, verwenden wir die Newtonschen Bewegungsgleichungen.

## Bewegungsgleichungen

Die Geschwindigkeit $v$ ist definiert als der zurückgelegte Weg $\Delta s$ geteilt durch die verstrichene Zeit $\Delta t$:

$$v = \frac{\Delta s}{\Delta t}$$

Die Beschleunigung $a$ ist definiert als die Geschwindigkeitsänderung $\Delta v$ geteilt durch die verstrichene Zeit $\Delta t$:

$$a = \frac{\Delta v}{\Delta t}$$

Die Beschleunigung ist die Kraft $F$ geteilt durch die Masse des Körpers $m$:

$$a = \frac{F}{m}$$

Wir interessieren uns im folgenden für die Änderung der Geschwindigkeit und der Position, also formen wir um:

$$\Delta v = \frac{F}{m}\cdot \Delta t \qquad \Delta s = v \cdot \Delta t$$

# Diskrete Simulation

## Iterative Berechnungen

Wenn die Kraft konstant ist, ergibt sich für die Geschwindigkeit eine lineare Funktion, für den Weg eine quadratische. Dies hat Galileo Galilei aufgrund von Experimenten für den freien Fall im Vakuum erstmals mathematisch beschrieben.

Beim freien Fall in einem Medium (z.B. Luft) sind die Kräfte aber nicht konstant. Deshalb können Geschwindigkeit und zurückgelegte Distanz nicht als Funktion beschrieben werden. Für diesen Fall wählt man ein **iteratives Näherungsverfahren**: Die Änderung von Geschwindigkeit und Distanz werden mit Hilfe der Formeln für eine sehr kurze Zeitspanne berechnet.


Wir gehen davon aus, dass die Geschwindigkeit $v_0$ und die Position $s_0$ zum Zeitpunkt $t_0$ bekannt sind. Dann können wird die Geschwindigkeit $v_1$ und die Position $s_1$ für den Zeitpunkt $t_1$ wie folgt berechnen:

1. Erst wird die verstrichene Zeit $\Delta t$ bestimmt:

  $$\Delta t = t_1 - t_0\qquad$$

2. Dann wird die Summe der Kräfte $F_1$ berechnet, welche zum Zeitpunkt $t_1$ auf das Objekt wirken.

  $$F_1 = F_A + F_B + F_C + \ldots$$

3. Nun wird die neue Geschwindigkeit berechnet:

  $$v_1 = v_0 + \frac{F_1}{m}\cdot \Delta t$$

4. Schliesslich wird die neue Position berechnet. Als Geschwindigkeit nehmen wir das Mittel  der Geschwindigkeiten zum Zeitpunkt $t_0$ und $t_1$ an:

$$s_1 = s_0 + \frac{v_0 + v_1}{2} \cdot \Delta t$$

Je kleiner die Zeitdifferenz ist, desto genauer wird die Berechnung.


## Freier Fall in der Luft

Beim freien Fall in der Luft wirkt zusätzlich die **Luftwiderstandskraft** $F_W$. Für sie gilt folgende Formel:

$$F_W = c_w \cdot A \cdot \frac{1}{2} \cdot \rho \cdot v^2$$

Dabei haben die Formelzeichen folgende Bedeutung:

| Zeichen | Wert                          | Bedeutung                               |
|:------- |:----------------------------- |:--------------------------------------- |
| $c_w$   | $0.45$                        | Luftwiderstandsbeiwert                  |
| $A$     | ?                             | Querschnittsfläche des fallenden Köpers |
| $\rho$  | $1.293\;\text{kg}/\text{m}^2$ | Luftdichte                              |
| $v$     | ?                             | Geschwindigkeit des fallenden Körpers   |

Wir betrachten eine fallende **Kugel**. Der entsprechende Luftwiderstandsbeiwert ist angegeben, der **Radius** der Kugel soll für die Berechnung gewählt werden können.

## Freier Fall im Wasser

Beim freien Fall in einem Medium wirkt auch die **Auftriebskraft** $F_A$. Sie entspricht der
umgekehrten Gravitationskraft des verdrängten Wassers. Die Masse des verdrängten Wassers berechnet sich wie folgt:

$$m_v = \rho \cdot V$$

Dabei ist $V$ das verdrängte Volumen und $\rho = 0.9982067$ die Dichte des Wassers. Diese Dichte muss übrigens auch für die Wasserwiderstandskraft verwendet werden.

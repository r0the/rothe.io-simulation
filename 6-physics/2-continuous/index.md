# Kontinuierliche Simulation

Wenn die Kraft konstant ist, ergibt sich für die Geschwindigkeit eine lineare Funktion, für den Weg eine quadratische. Dies hat Galileo Galilei aufgrund von Experimenten für den freien Fall **im Vakuum** erstmals mathematisch beschrieben.

$$ g = \frac{F}{m} = 9.81 m/s^2$$

## Validierung

David Randolph Scott validiert 1971 im Vakuum der Mondoberfläche die These von Galilei, dass im Vakuum alle Objekte gleich schnell fallen.

<VueVideo id="ZVfhztmK9zI"/>

::: exercise
#### ✏️ Galileo Galilei
1. Berechne in Excel die Falldistanz eines Objeks im Vakuum zu verschiedenen Zeiten.
2. Erstelle ein Diagramm davon.
:::

## Fall auf dem Mond

Beim freien Fall im Vakuum wirkt nur eine Kraft, die **Gravitationskraft** $F_G$. Sie wird durch folgende Formel bestimmt:

$$F_G = G \cdot \frac{m \cdot M}{r^2}$$

Dabei haben die Formelzeichen folgende Bedeutung:

| Zeichen | Wert                     | Bedeutung                      |
|:------- |:------------------------ |:------------------------------ |
| $G$     | $6.67408 \cdot 10^{-11}$ | Gravitationskonstante          |
| $m$     | wählbar                  | Masse des einen Körpers        |
| $M$     | ?                        | Masse des anderen Körpers      |
| $r$     | ?                        | Abstand der zwei Massenzentren |

::: exercise
#### ✏️ Apollo 15
1. Berechne die Fallbeschleunigung auf der Mondoberfläche.
2. Erweitere die letzte Berechnung um die Falldistanz auf dem Mond.
3. Ergänze das Diagramm.
:::

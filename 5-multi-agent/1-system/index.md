# Modellierung 1 - System[^1]
---

Wir verwenden für diese Reihe ein sehr einfaches Modell eines eher harmlosen Virus. Für unser fiktives Szenario ist es unerheblich, ob es sich um ein Virus bei Menschen oder Tieren handelt. Daher sprechen wir im Folgenden immer von Individuen. In den Aufgaben werden Variationen dieses Modells vorgeschlagen. Ihr solltet jedoch darauf aufbauend versuchen, euer eigenes Modell zu entwickeln und zu implementieren.

## Unser Modell

In unserem Modell sollen sich 50 Individuen in einem Raum frei und zufällig bewegen. Die Infizierung und Erkrankung geschieht gemäss einem zustandsbasierten Modell, das im nächsten Schritt beschrieben wird. Abschliessend erfolgt als statistische Aufbereitung eine Zählung und grafische Darstellung, so dass die zeitliche Entwicklung der Ausbreitung nachvollzogen werden kann.

## Raum und Anordnung

Der Raum wird als Rechteck modelliert, dessen Koordinaten wir in Abhängigkeit von der Programmiersprache und der Anzahl der Individuen wählen. Jedes Individuum soll in diesem Rechteck zufällig platziert werden. Dabei sollte die Wahrscheinlichkeit der Zuordnung zu einer Position innerhalb des Rechteckes gleichverteilt sein.

![Illustration des Modells](./space-movement.jpg)

## Bewegung

Die Bewegung der Individuen soll gleichförmig erfolgen, d. h. die Individuen bewegen sich mit konstanter Geschwindigkeit immer in die gleiche Richtung, am Rand des Rechtecks «prallen sie ab» (Einfallswinkel gleich Ausfallswinkel). Alle Individuen bewegen sich mit gleicher Geschwindigkeit und weichen nicht einander aus.

[^1]: Quelle: [inf-schule.de, CC BY-SA-4.0](https://www.inf-schule.de/vernetzung/simulationen)

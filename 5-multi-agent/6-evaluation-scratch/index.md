# Scratch - Auswertung[^1]
---

In Scratch soll im unteren abgetrennten Bereich ein Diagramm erstellt werden, das die Anzahl der einzelnen Gruppen (Krankheits-Zustände) in Abhängigkeit von der Zeit darstellt. Dazu soll jede Sekunde für jeden Zustand ein Pixel für den entsprechenden Punkt (Zeit;Anzahl) des Grafen erzeugt werden.

![Scratch-Bühne mit Plot](./scratch-evaluation.jpg)

## Zählvariablen

Zunächst müssen dazu zu jedem Zustand die entsprechenden Zählvariablen erstellt und initialisiert werden. Um den Grafen in Abhängigkeit von der Zeit sinnvoll zeichnen zu können, muss beim Start die Stoppuhr zurückgesetzt werden. Die Initialisierung erfolgt zweckmässig im Start-Skript der Bühne:

``` scratch
Wenn die grüne Flagge angeklickt
setze [Anzahl Kranke v] auf (0)
wiederhole (49) mal
erzeuge Klon von (Person v)
ende
wiederhole fortlaufend
warte (0.1) Sekunden
sende (Bewege v) an alle
ende
```

Bei jedem Zustandsübergang muss dann auch die Anzahl durch Dekrementierung und Inkrementierung entsprechend angepasst werden. Um Fehler durch einen synchronen Ablauf mit der Initialisierung zu verhindern muss hier zu Beginn (im Block Startposition) ein weiterer Zeitpuffer eingebaut werden.

``` scratch
Definiere Krankheitsverlauf
wechsle zu Kostüm (infiziert v)
warte (6) Sekunden
wechsle zu Kostüm (erkrankt v)
ändere [Anzahl Kranke v] um (1)
warte (14) Sekunden
ändere [Anzahl Kranke v] um (-1)
wechsle zu Kostüm (geheilt v)
```

## Plotten der Zuordnungsgrafen

Zur grafischen Aufzeichnung benötigen wir vier Malobjekte, um die Grafen der jeweiligen Anzahlen in Abhängigkeit von der Zeit zu plotten. Der Ursprung des Koordinatensystems soll auf dem Bildschirmpunkt (-240,-175) liegen, so dass der Plot im unteren abgetrennten Bereich verbleibt. Beim Start der Simulation soll die alte Zeichnung gelöscht werden und die Zeichenstifte (Malobjekte) müssen auf ihre Startposition gesetzt werden. Nachdem die Zeit, die zu Beginn für das Klonen und Initialisieren benötigt wird, vorüber ist, bewegt sich der Stift z. B. jede Sekunde (das entspricht bei unserer Simulationen der Veränderung an einem Tag) an seine neue Position um den Grafen schrittweise zu plotten.

Nachfolgend als Beispiel das Skript für das Plotten der Zuordnung von Laufzeit zur Anzahl der Infizierten.

``` scratch
Wenn die grüne Flagge angeklickt
lösche alles
setze Stiftdicke auf (3)
setze Stiftfarbe auf (#000000)
schalte Stift aus
warte (3) Sekunden
wiederhole fortlaufend
gehe zu x: ((-240) + ((Stopuhr) * (4))) y: ((-175) + (Anzahl Kranke))
warte (0.5) Sekunden
```

::: exercise Aufgaben
1. Beschreibe den Verlauf der Grafen:
    - Welche charakteristischen Merkmale kannst du erkennen?
    - Kannst du die Grafen einem Funktionstyp, den du aus dem Mathematik-Unterricht kennst, zuordnen?
    - Erkläre den Verlauf auf der Basis der Modellannahmen.
2. Führe die Simulation mehrfach durch: Welche Unterschiede und Gemeinsamkeiten bei den einzelnen Durchgängen kannst du beobachten?
3. Verändere einige Paramter der Simulation, z. B. die Anzahl der Klone, die Zeitdauer beim Zustandsmodell oder die Geschwindigkeit der Bewegungen. Erkläre die Auswirkungen der jeweiligen Änderungen.
:::

[^1]: Quelle: [inf-schule.de, CC BY-SA-4.0](https://www.inf-schule.de/vernetzung/simulationen)

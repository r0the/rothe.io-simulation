# Validierung
---

Einen Schritt zur Validierung – nämlich die Darstellung des Zustandes der Personen – haben wir bereits im vorherigen Kapitel erledigt. Das reicht bei Weitem nicht aus für eine seriöse Auswertung.

Hilfreich wäre ein Diagramm, das die Ansteckungen im Verlaufe der Zeit zeigt. So könnten wir jeweils nach einer gewissen Zeit das Diagramm speichern und so die Verläufe etlicher Simulationsdurchgänge vergleichen und auswerten.

## Graphen zeichnen mit matplotlib

Mit der Python-Bibliothek `matplotlib` können wir bequem Werte in Koordinatensystemen visualisieren. Aus dieser umfangreichen Bibliothek verwenden wir nur `pyplot`, daher können wir den wie folgt formulieren:

``` python
import matplotlib.pyplot as plot
```

Um zu vermeiden, dass wir jedem `pyplot`-Befehl `matplotlib.pyplot` voranstellen müssen (wie dies sonst üblich ist beim Importieren aus Python-Bibliotheken), definieren wir nach dem Schlüsselwort `as` einen eigenen Namen (hier `plot`). Diesen können wir als Abkürzung in den nachfolgenden Aufrufen verwenden.

Die einfachste Art für unsere Simulation, Graphen zu zeichnen, ist das Verwenden einer Liste mit Zahlen:

``` python
import matplotlib.pyplot as plot

liste = [1, 13, 25, 11, 9, 18, 5]

plot.plot(liste)
plot.show()
```

Die Funktion `plot()`, visualisiert die Liste, indem sie die in der Liste enthaltenen Werte als Funktionswerte oder y-Koordinate auffasst. Als zugehörige x-Koordinate wird jeweils der Index des entsprechenden Elementes verwendet. Der Aufruf der Funktion `show()` zeigt schliesslich ein separates Fenster an, das den Plot enthält.

Das vorherige Beispiel ergibt folgende Grafik:

![Einfacher Plot](./images/plot-1.png)

## Graphen gestalten

Selbstverständlich begnügen wir uns nicht mit dieser simplen Ausgabe. Es fehlen Titel, Achsenbeschriftungen sowie eine Legende. Dies alles ist mit einigen weiteren Befehlen leicht zu erreichen:

``` python
import matplotlib.pyplot as plot

liste1 = [1, 13, 25, 11, 9, 18, 5]
liste2 = [2, 4, 5, 1, 7, 8, 3]

plot.plot(liste1, label='Hirsche')
plot.plot(liste2, label='Hasen')
plot.xlabel('Tag')
plot.ylabel('Anzahl')
plot.title("Tierbeobachtungen")
plot.legend()
plot.show()
```

Die Befehle sind selbsterklärend. Es ist zudem ersichtlich, dass mehrere Datenreihen im selben Plot dargestellt werden können. Nachfolgend findest du die entsprechende Ausgabe:

![Plot mit Beschriftungen](./images/plot-2.png)

## Farben

Wenn nicht sie nicht vorgegeben werden, werden die Farben den Datenreihen automatisch zugewiesen. Für unsere Anwendung ist es sicher sinnvoll, die gleichen Farben im Diagramm wie in der Simulation zu verwenden.

Analog zum Label (also der Beschriftung der Datenreihen) kann auch die Farbe der `plot()`-Funktion mitgegeben werden:

``` python
plot.plot(liste1, label='Hirsche', color='red')
plot.plot(liste2, label='Hasen', color='blue')
```

Die üblichen Farben können direkt mit dem Namen ausgewählt werden. Eine Liste mit weiteren Namen findet man [hier](https://matplotlib.org/3.1.0/gallery/color/named_colors.html?highlight=named%20colors).

Zudem können die Farben auch als RGB-Werte `(r, g, b)` eingegeben werden – allerdings müssen die einzelnen Werte (anders als by Pygame Zero) zwischen 0 und 1 liegen (anstatt zwischen 0 und 255).

## Diagramme speichern

Das Menü unterhalb des Plots erlaubt die Speicherung als PNG-Datei (Klick auf das Diskettensymbol).

![Symbolleiste](./images/plot-menu.png)

::: exercise Aufgabe – Validierung
**Ausgangslage anpassen**:
- Überlege dir, was den Ausgang der Simulation beeinflusst, und erstelle für diese Werte – falls nicht bereits erfolgt – Konstanten, damit du die Werte einfacher anpassen kannst.
- Spiele mit den Werten und schaue dir die Auswirkungen an.
- Stelle die Simulation so ein, dass meist ein deutlicher Teil der Personen infiziert wird.

**Graphen erstellen**:
- Erstelle eine weitere Liste, die für jeden Tag die Anzahl der Infizierten speichert.
- Lass die Simulation abbrechen, sobald die Anzahl der Infizierten `0` ist. Erstelle zu diesem Zeitpunkt ein Diagramm der Infizierten mit der matplotlib.
- Erweitere die Simulation mit einer weiteren Liste für die Anzahl der Erkrankten pro Tag. Brich die Simulation erst ab, wenn beide – die Anzahl der Infizierten und Erkrankten – auf `0` gesunken ist.
- Anschliessend meldest du dich unbedingt bei mir, bevor du mit dem Verfeinern des Modells weiterfährst.
:::


::: exercise Zusatzaufgaben – Erweiterung des Modells
Wähle jeweils **nur einen Punkt** aus folgender Liste aus, um dein Modell zu verfeinern, die Simulation anzupassen und auszuwerten. Anschliessend kannst du den nächsten Punkt verbessern.

Ideen für die weitere Verfeinerung des Modells (in aufsteigender Schwierigkeit):

- Die Infektion soll nicht bei jedem Kontakt erfolgen, sondern nur mit einer gewissen Wahrscheinlichkeit.
- Erweitere das Zustandsdiagramm so, dass auch Todesfälle einbezogen werden und mit einer gewissen Wahrscheinlichkeit nach einer Krankheit vorkommen.
- Um die Auswertung zu erleichtern, soll zu jedem Zeitpunkt mittels Textausgabe ersichtlich sein, wie viele Personen sich in welchem Zustand befinden.
- Zeige im Diagramm nicht nur die Infizierten und Erkrankten, sondern auch die Gesunden, Geheilten und Verstorbenen an.
:::

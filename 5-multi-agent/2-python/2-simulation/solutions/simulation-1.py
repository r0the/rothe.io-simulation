import pgzrun
import random

TITLE = "Infektionssimulation"
WIDTH = 1200
HEIGHT = 600

ANZAHL = 100

# Farben
FARBE_HINTERGRUND = (180, 180, 180)

# Grösse
RADIUS = 4

personen_x = []
personen_y = []
personen_vx = []
personen_vy = []

# Anfangszustand
for i in range(0, ANZAHL):
    personen_x.append(random.randrange(0, WIDTH))
    personen_y.append(random.randrange(0, HEIGHT))
    personen_vx.append(random.randrange(-2, 2))
    personen_vy.append(random.randrange(-2, 2))


def draw():
    screen.fill(FARBE_HINTERGRUND)
    
    # Personen zeichnen
    for i in range(0, ANZAHL):
        screen.draw.filled_circle((personen_x[i], personen_y[i]), RADIUS, (0, 0, 0))


def update(zeitdifferenz):
    for i in range(0, ANZAHL):
        # bewegen
        personen_x[i] += personen_vx[i]
        personen_y[i] += personen_vy[i]
    
        if personen_x[i] < 0 or personen_x[i] > WIDTH:
            personen_vx[i] = personen_vx[i] * -1
        
        if personen_y[i] < 0 or personen_y[i] > HEIGHT:
            personen_vy[i] = personen_vy[i] * -1


pgzrun.go()

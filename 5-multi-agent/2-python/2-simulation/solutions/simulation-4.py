import pgzrun
import random
import math
import matplotlib.pyplot as plot

TITLE = "Infektionssimulation"
WIDTH = 1200
HEIGHT = 600

ANZAHL = 100

# Zustände
GESUND = 0
INFIZIERT = 1
ERKRANKT = 2
GEHEILT = 3
VERSTORBEN = 4

# Zeiten
ZEIT_EINHEITEN = 10                   # pro Tag
INKUBATIONSZEIT = 6 * ZEIT_EINHEITEN
KRANKHEITSZEIT = 14 * ZEIT_EINHEITEN

# Veranlagung
STERBEWAHRSCHEINLICHKEIT = 20
INFEKTIONSRISIKO = 50

# Ansteckungsdistanz
ANSTECKUNGSDISTANZ = 20

# Farben
FARBE_HINTERGRUND = (180, 180, 180)
FARBE_WEISS = (255, 255, 255)
FARBE_GESUND = (0, 138, 0)
FARBE_INFIZIERT = (255, 255, 0)
FARBE_ERKRANKT = (255, 0, 0)
FARBE_GEHEILT = (0, 0, 255)

# Grösse
RADIUS = 4

personen_x = []
personen_y = []
personen_vx = []
personen_vy = []
personen_zustand = []
personen_zeit = []

zeit = 0

# Anfangszustand
for i in range(0, ANZAHL):
    personen_x.append(random.randrange(0, WIDTH))
    personen_y.append(random.randrange(0, HEIGHT))
    personen_vx.append(random.randrange(-2, 2))
    personen_vy.append(random.randrange(-2, 2))
    personen_zustand.append(GESUND)
    personen_zeit.append(0)

# erste Infizierte Person
zahl = random.randrange(0, ANZAHL)
personen_zustand[zahl] = INFIZIERT

# Statistik
anzahl_gesunde = [ANZAHL - 1]
anzahl_infizierte = [1]
anzahl_erkrankte = [0]
anzahl_geheilte = [0]
anzahl_verstorbene = [0]
ansteckungsglueck = [0]

fertig = False


def draw():
    screen.fill(FARBE_HINTERGRUND)
    
    # Personen zeichnen
    for i in range(0, ANZAHL):
        farbe = FARBE_GESUND
        
        if personen_zustand[i] == INFIZIERT:
            farbe = FARBE_INFIZIERT
        elif personen_zustand[i] == ERKRANKT:
            farbe = FARBE_ERKRANKT
        elif personen_zustand[i] == GEHEILT:
            farbe = FARBE_GEHEILT
        
        if personen_zustand[i] != VERSTORBEN:
            screen.draw.filled_circle((personen_x[i], personen_y[i]), RADIUS, farbe)
    
    # aktuellen Stand anzeigen
    screen.draw.text("Gesunde: " + str(anzahl_gesunde[-1]), right = WIDTH, bottom = HEIGHT - 100)
    screen.draw.text("Infizierte: " + str(anzahl_infizierte[-1]), right = WIDTH, bottom = HEIGHT - 80)
    screen.draw.text("Erkrankte: " + str(anzahl_erkrankte[-1]), right = WIDTH, bottom = HEIGHT - 60)
    screen.draw.text("Geheilte: " + str(anzahl_geheilte[-1]), right = WIDTH, bottom = HEIGHT - 40)
    screen.draw.text("Gestorbene: " + str(anzahl_verstorbene[-1]), right = WIDTH, bottom = HEIGHT - 20)
    screen.draw.text("Ansteckungsglück: " + str(ansteckungsglueck[-1]), right = WIDTH, bottom = HEIGHT)
    
    # Plot
    if anzahl_infizierte[-1] == 0 and anzahl_erkrankte[-1] == 0:
        plot.title('Infektionsverlauf')
        plot.xlabel('Zeit')
        plot.ylabel('Anzahl')
        plot.plot(anzahl_gesunde, label='Gesunde', color='green')
        plot.plot(anzahl_infizierte, label='Infizierte', color='yellow')
        plot.plot(anzahl_erkrankte, label='Erkrankte', color='red')
        plot.plot(anzahl_geheilte, label='Geheilte', color='blue')
        plot.plot(anzahl_verstorbene, label='Verstorbene', color='black')
        plot.legend()
        plot.show()
        fertig = True


def update(zeitdifferenz):
    global zeit
    
    if fertig:
        return
    
    zeit += 1

    # Zustand von letzter Zeiteinheit übernehmen
    anzahl_gesunde.append(anzahl_gesunde[-1])
    anzahl_infizierte.append(anzahl_infizierte[-1])
    anzahl_erkrankte.append(anzahl_erkrankte[-1])
    anzahl_geheilte.append(anzahl_geheilte[-1])
    anzahl_verstorbene.append(anzahl_verstorbene[-1])
    ansteckungsglueck.append(0)
    
    for i in range(0, ANZAHL):
        # Gesundheit prüfen
        if personen_zustand[i] == GESUND:
            for j in range(0, ANZAHL):
                distanz = math.sqrt((personen_x[i] - personen_x[j])**2 + (personen_y[i] - personen_y[j])**2)
                if (personen_zustand[j] == INFIZIERT or personen_zustand[j] == ERKRANKT) and distanz < ANSTECKUNGSDISTANZ:
                    infektion = random.randrange(0, 100)
                    if infektion < INFEKTIONSRISIKO:
                        personen_zustand[i] = INFIZIERT
                        anzahl_gesunde[zeit] -= 1
                        anzahl_infizierte[zeit] += 1
                        break
                    else:
                        ansteckungsglueck[zeit] += 1
        elif personen_zustand[i] == INFIZIERT:
            if personen_zeit[i] > INKUBATIONSZEIT:
                personen_zustand[i] = ERKRANKT
                anzahl_infizierte[zeit] -= 1
                anzahl_erkrankte[zeit] += 1
                personen_zeit[i] = 0
            else:
                personen_zeit[i] += 1
        elif personen_zustand[i] == ERKRANKT:
            if personen_zeit[i] > KRANKHEITSZEIT:
                risiko = random.randrange(0, 100)
                if risiko < STERBEWAHRSCHEINLICHKEIT:
                    personen_zustand[i] = VERSTORBEN
                    anzahl_erkrankte[zeit] -= 1
                    anzahl_verstorbene[zeit] += 1
                else:
                    personen_zustand[i] = GEHEILT
                    anzahl_erkrankte[zeit] -= 1
                    anzahl_geheilte[zeit] += 1
            else:
                personen_zeit[i] += 1
        
        # bewegen
        personen_x[i] += personen_vx[i]
        personen_y[i] += personen_vy[i]
    
        if personen_x[i] < 0 or personen_x[i] > WIDTH:
            personen_vx[i] = personen_vx[i] * -1
        
        if personen_y[i] < 0 or personen_y[i] > HEIGHT:
            personen_vy[i] = personen_vy[i] * -1


pgzrun.go()

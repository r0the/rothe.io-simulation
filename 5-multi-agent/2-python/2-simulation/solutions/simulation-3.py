import pgzrun
import random
import math
import matplotlib.pyplot as plot

TITLE = "Infektionssimulation"
WIDTH = 1200
HEIGHT = 600

ANZAHL = 100

# Zustände
GESUND = 0
INFIZIERT = 1
ERKRANKT = 2
GEHEILT = 3

# Zeiten
ZEIT_EINHEITEN = 10                   # pro Tag
INKUBATIONSZEIT = 6 * ZEIT_EINHEITEN
KRANKHEITSZEIT = 14 * ZEIT_EINHEITEN

# Ansteckungsdistanz
ANSTECKUNGSDISTANZ = 20

# Farben
FARBE_HINTERGRUND = (180, 180, 180)
FARBE_GESUND = (0, 138, 0)
FARBE_INFIZIERT = (255, 255, 0)
FARBE_ERKRANKT = (255, 0, 0)
FARBE_GEHEILT = (0, 0, 255)

# Grösse
RADIUS = 4

personen_x = []
personen_y = []
personen_vx = []
personen_vy = []
personen_zustand = []
personen_zeit = []

zeit = 0

# Anfangszustand
for i in range(0, ANZAHL):
    personen_x.append(random.randrange(0, WIDTH))
    personen_y.append(random.randrange(0, HEIGHT))
    personen_vx.append(random.randrange(-2, 2))
    personen_vy.append(random.randrange(-2, 2))
    personen_zustand.append(GESUND)
    personen_zeit.append(0)

# erste Infizierte Person
zahl = random.randrange(0, ANZAHL)
personen_zustand[zahl] = INFIZIERT

# Statistik
anzahl_infizierte = [1]
anzahl_erkrankte = [0]



def draw():
    screen.fill(FARBE_HINTERGRUND)
    
    # Personen zeichnen
    for i in range(0, ANZAHL):
        farbe = FARBE_GESUND
        
        if personen_zustand[i] == INFIZIERT:
            farbe = FARBE_INFIZIERT
        elif personen_zustand[i] == ERKRANKT:
            farbe = FARBE_ERKRANKT
        elif personen_zustand[i] == GEHEILT:
            farbe = FARBE_GEHEILT
        
        screen.draw.filled_circle((personen_x[i], personen_y[i]), RADIUS, farbe)
    
    # Plot
    if anzahl_infizierte[-1] == 0 and anzahl_erkrankte[-1] == 0:
        plot.title('Infektionsverlauf')
        plot.xlabel('Zeit')
        plot.ylabel('Anzahl')
        plot.plot(anzahl_infizierte, label='Infizierte', color='yellow')
        plot.plot(anzahl_erkrankte, label='Erkrankte', color='red')
        plot.legend()
        plot.show()


def update(zeitdifferenz):
    global zeit
    
    zeit += 1

    # Zustand von letzter Zeiteinheit übernehmen
    anzahl_infizierte.append(anzahl_infizierte[-1])
    anzahl_erkrankte.append(anzahl_erkrankte[-1])
    
    for i in range(0, ANZAHL):
        # Gesundheit prüfen
        if personen_zustand[i] == GESUND:
            for j in range(0, ANZAHL):
                distanz = math.sqrt((personen_x[i] - personen_x[j])**2 + (personen_y[i] - personen_y[j])**2)
                if (personen_zustand[j] == INFIZIERT or personen_zustand[j] == ERKRANKT) and distanz < ANSTECKUNGSDISTANZ:
                    personen_zustand[i] = INFIZIERT
                    anzahl_infizierte[zeit] += 1
                    break
        elif personen_zustand[i] == INFIZIERT:
            if personen_zeit[i] > INKUBATIONSZEIT:
                personen_zustand[i] = ERKRANKT
                anzahl_infizierte[zeit] -= 1
                anzahl_erkrankte[zeit] += 1
                personen_zeit[i] = 0
            else:
                personen_zeit[i] += 1
        elif personen_zustand[i] == ERKRANKT:
            if personen_zeit[i] > KRANKHEITSZEIT:
                personen_zustand[i] = GEHEILT
                anzahl_erkrankte[zeit] -= 1
            else:
                personen_zeit[i] += 1
        
        # bewegen
        personen_x[i] += personen_vx[i]
        personen_y[i] += personen_vy[i]
    
        if personen_x[i] < 0 or personen_x[i] > WIDTH:
            personen_vx[i] = personen_vx[i] * -1
        
        if personen_y[i] < 0 or personen_y[i] > HEIGHT:
            personen_vy[i] = personen_vy[i] * -1


pgzrun.go()

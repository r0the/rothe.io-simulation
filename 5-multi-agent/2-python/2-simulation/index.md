# Simulation
---

Für die Programmierung verwenden wir die bereits vertraute Programmierumgebung Thonny mit Python und Pygame Zero.

## Umsetzung des Bewegungsmodells

- Die **Personen** werden durch farbige Kreise dargestellt.
- Die Farbe zeigt den **Zustand** der Person an.
- Die Personen bewegen sich im gesamten Raum **geradlinig** und **gleichförmig**. Falls sie die Raumbegrenzung erreichen, «prallen sie ab» (wie ein Ball).

**Tipp**: Die Animation des Schneefalls mit den Listen zur Speicherung der Positions- und Grössenangaben kann als Vorlage für die Simulation dienen.

::: exercise Aufgabe – Umsetzung des Bewegungsmodells
Erstelle auf Basis der Schneefall-Animation folgende Animation:

- Bereite Listen für die Speicherung der Positionsdaten vor.
- Da sich die Bewegungsrichtung beim «Abprallen» teilweise umkehrt, muss die Änderung in x- und y-Richtung (= Geschwindigkeit) für jede Person separat gespeichert werden. Auch dazu eignen sich Listen bestens.
- Beim Start werden 100 zufällige Positionen und Geschwindigkeiten berechnet und in den Listen gespeichert.
- In der `draw()`-Funktion werden die Personen an ihrer aktuellen Position gezeichnet.
- In der `update()`-Funktion werden die Personen bewegt.
- Zudem wird in der `update()`-Funktion nach dem Bewegen überprüft, um die Person die Richtung ändern muss, weil sie einen Rand erreicht hat.

Du darfst erst weiterfahren, wenn die Personen korrekt platziert werden und sich im Raum gemäss den Vorgaben bewegen.
***
``` python solutions/simulation-1.py
```
:::

## Umsetzung des Infektionsmodells

::: exercise Aufgabe – Vorbereitung der Umsetzung des Modells
Überlege dir mit deiner Pultnachbarin/deinem Pultnachbarn Folgendes:

- Wie speichert ihr die Zustände der einzelnen Personen und wie stellt ihr diese grafisch dar?
- Wie findet ihr heraus, ob zwei Personen Kontakt haben?
- Wie weiss das Programm, welche Person bereits wie lange infiziert resp. krank ist?
- Wie realisiert ihr den Start der Epidemie?

Bevor ihr mit Programmieren beginnt, müsst ihr eure Vorstellungen mit mir besprechen.
:::

::: exercise Aufgabe – Umsetzung des Modells
- Programmiere die Simulation. <br>**Hinweis**: Um die Zeit in der `update()`-Funktion zu aktualisieren, muss zu oberst in der `update()`-Funktion die Zeile `global zeit` eingefügt werden.
- Falls sie zu deiner Zufriedenheit funktioniert, lies unter «Validierung» weiter.
:::

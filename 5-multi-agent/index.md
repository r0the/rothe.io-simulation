# Einstieg – Pandemien[^1]
---

## Coronavirus

![Illustration eines Coronavirus ©](./sars-cov-2.png)

Im Jahr 2020 sorgte die Corona-Pandemie für mehrwöchige Schulschliessungen. Die rasche Ausbreitung und grosse Infektionsrisiken machten enorme Einschränkungen des Alltags und ausserordentliche Hygienemassnahmen notwendig. Weltweit gab es über eine Million Todesfälle (Stand am 11.10.2020: 1'070'355[^2]).

Neben den Schulen mussten daher auch viele Geschäfte und Unternehmen ihren Betrieb einstellen oder stark zurückfahren. Persönliche Kontakte waren auf die dringend notwendigen zu reduzieren. Damit sollte einer noch schnelleren Verbreitung entgegengewirkt werden und somit zur Entlastung der Krankenhäuser beigetragen werden.

**Pandemien** bezeichnen länderübergreifende Ausbreitungen von Infektionskrankheiten. Die Ausbreitung des Corona-Virus SARS-CoV-2, die vermutlich im Dezember 2019 in China begann, wurde am 11. März 2020 von der Weltgesundheitsorganisation WHO zur Pandemie erklärt.

## Simulation einer Ausbreitung

In diesem Kapitel wollen wir die Ausbreitung eines Virus simulieren. Konkret werden wir mit einem einfachen Modell die Ausbreitung eines Virus nachahmen, veranschaulichen und statistisch erfassen.

Eine **Simulation** bezeichnet die Analyse eines dynamischen Systems durch Experimente an einem Modell, um Erkenntnisse über das reale System zu erzielen.

Simulationen werden in der Regel dort verwendet, wo man die Zusammenhänge relevanter Grössen nicht mehr mit einfachen mathematischen Formeln beschreiben kann. Häufig spielt bei solchen Systemen auch die Modellierung des Zufalls (stochastische Prozesse) eine Rolle.

Der erste Schritt einer Simulation ist die Modellfindung.

::: exercise Aufgaben
1. Welche Daten können für die Simulation der Ausbreitung eines Virus relevant sein?
2. Recherchiere zu bekannten Viren/Pandemien (Corona, Schweinegrippe, Vogelgrippe H5N1, …): Welche Fakten zur Verbreitung wurden wo veröffentlicht?
3. Überlegt euch, welche der genannten Fakten in eurer Simulation eine Rolle spielen sollen.
:::

[^1]: Quelle: [inf-schule.de, CC BY-SA-4.0](https://www.inf-schule.de/vernetzung/simulationen)
[^2]: Quelle: [Weltgesundheitsorganisation WHO](https://who.sprinklr.com/)

# Modellierung 2 - Infektion[^1]

Wie bereits erwähnt, modellieren wir ein sehr einfaches Modell eines eher harmlosen Virus.

![Zustandsdiagramm Krankheitsverlauf](./state-diagram-infection.svg)

Wir gehen in unserem Modell davon aus, dass sich ein Individuum immer in einem der folgenden vier Zustände befindet:

1. gesund
2. infiziert
3. erkrankt
4. geheilt

Die Infizierung erfolgt immer sofort bei einem Kontakt mit einer bereits infizierten oder einer erkrankten Person. Nach der Infizierung dauert es exakt 6 Tage bis die Krankheit ausbricht. Nach 14 weiteren Tagen gelten die Personen dann als geheilt. Eine erneute Erkrankung ist nicht mehr möglich. Dieser Teil der Modellierung ist im Zustandsdiagramm rechts dargestellt.

Unser Modell enthält noch keine Maßnahmen zur Reduzierung oder Vermeidung von Kontakten wie Ausgangssperren oder Quarantäne.

::: exercise Aufgaben
Zeichne jeweils ein verändertes Zustandsmodell so, dass
1. Kranke nach ihrer Gesundung wieder infiziert werden könnten,
2. bei 30% der Infizierten die Krankheit nicht ausbricht (diese gelten hier nach 6 Tagen als geheilt),
3. 2% der Erkrankten an der Krankheit versterben.
:::

[^1]: Quelle: [inf-schule.de, CC BY-SA-4.0](https://www.inf-schule.de/vernetzung/simulationen)

# Auswertung
---

Um die Entwicklung der Zahl der Infizierten, Kranken und Gesundeten zu veranschaulichen ist eine statistische Erfassung und Auswertung notwendig. Hierzu benötigen wir Variablen um die Anzahl der Individuen in den jeweiligen Zuständen zu zählen. Die Variablen sollten immer sofort beim Zustandsübergang de- bzw. inkrementiert werden, so dass immer der aktuelle Bestand in den Variablen erfasst ist.

**Beispiel bei 50 Individuen zu einem Zeitpunkt x**

- anzahl_gesunde = 13
- anzahl_infizierte = 16
- anzahl_kranke = 14
- anzahl_geheilte = 7

Um die Entwicklung aufzuzeichnen kann man (in Echtzeit) einen Grafen erstellen, indem in festen Zeitabständen ein Bildpunkt im Koordinatensystem erzeugt wird. - Alternativ kann man die Werte auch zunächst in entsprechenden Datenstrukturen (zum Beispiel Feldern) speichern, um sie später in vielfältiger Weise darstellen und auswerten zu können.

Beispiel-Graph einer Zuordnung Zeit zu Anzahl

![Graph einer Zuordnung Zeit-Anzahl](./graph-time-amount.svg)

---
Quelle: [inf-schule.de, CC BY-SA-4.0](https://www.inf-schule.de/vernetzung/simulationen)

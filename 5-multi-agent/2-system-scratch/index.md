# Implementierung Scratch 1 - System[^1]
---

* [:link: Scratch-Editor](https://scratch.mit.edu/projects/editor/)

* [:link: Scratch-Vorlage](https://scratch.mit.edu/projects/490477551)

## Bühne

Die Scratch-Welt heisst «Bühne» und ist eine Fläche von 480x360 Pixel. Die x-Achse liegt im Bereich -240 bis 240, die y-Achse im Bereich -180 bis 180. Der Ursprung (0, 0) ist die Mitte der Bühne.

![](./scratch-stage.svg)

## Personen

Für die Darstellung der Personen wird eine Figur mit dem Namen «Person» verwendet. Die Figur ist in der Vorlage bereits vorhanden. Die Figur wird als kleiner blauer Punkt dargestellt.

## Programm starten

Ein Scratch-Projekt wird mit einem Klick auf die grüne Fahne gestartet. Um Befehle beim Start auszuführen, wird der entsprechende Programmblock aus dem Bereich __Ereignisse__ verwendet:

``` scratch
Wenn die grüne Flagge angeklickt
```

## Verstecken

Die Person wollen wir nicht auf der Bühne sehen, also verstecken wir sie mit dem Befehl aus dem Bereich __Aussehen__:


``` scratch
Wenn die grüne Flagge angeklickt
verstecke dich
```

## Klonen

Da wird viele Personen benötigen, klonen wir unsere Person mit folgenden Befehlen aus dem Bereich __Steuerung__:

``` scratch
Wenn die grüne Flagge angeklickt
verstecke dich
wiederhole (200) mal
  erzeuge Klon von (mir selbst v)
ende
```

## Verhalten der Klone

Nun wollen wir das Verhalten der Klone definieren. Dazu beginnen wir einen neuen Code-Block mit dem folgenden Block aus dem Bereich __Steuerung__:

``` scratch
Wenn ich als Klon entstehe
```

## Eigene Befehle

Unsere Simulation wird recht umfangreich. Um den Überblick zu wahren, strukturieren wir unser Programm mit selbst definierten Blöcken (Unterprogramme). Diese werden im Bereich __Meine Blöcke__ erzeugt und verwaltet.

Wir definieren zwei Blöcke __Initialisierung_ und _Simulationsschritt__.

::: columns 2
``` scratch
Definiere Initialisierung
```
***

``` scratch
Definiere Simulationsschritt
```
:::

## Simulation

Nun definieren wir das grundlegende Verhalten der Simulation. Jeder Klon wird zunsächst initialisiert und führt dann fortlaufen einen Simulationsschritt aus:

``` scratch
Wenn ich als Klon entstehe
Initialisierung :: custom
wiederhole fortlaufend
Simulationsschritt :: custom
ende
```

## Initialisierung

Die Person soll an einer zufälligen Position auf der Bühne starten und in eine zufällige Richtung schauen. Ausserdem sollen die Klone sichtbar sein:


``` scratch
Definiere Initialisierung
zeige dich
gehe zu (Zufallsposition v)
setze Richtung auf (Zufallszahl von (0) bis (359)) Grad
```

## Bewegung

Nachdem ein Klon initialisiert ist, soll er sich auf der Bühne bewegen und am Rand «abprallen»:

``` scratch
Definiere Simulationsschritt
gehe (3) er Schritt
pralle vom Rand ab
```



[^1]: Quelle: [inf-schule.de, CC BY-SA-4.0](https://www.inf-schule.de/vernetzung/simulationen)

# Implementierung Scratch 2 - Infektion[^1]
---

## Zustände

Die vier Zustände lassen sich in Scratch sehr einfach durch entsprechende Kostüme darstellen.

## Infektionsherd

Bei der Erzeugung der Klone müssen wir dafür sorgen, dass zumindest ein infizierter Klon erzeugt wird. Anstelle von 200 Klonen erstellen wir nun also 199 Klone mit dem Kostüm «gesund» und einen Klon mit dem Kostüm «infiziert»:

``` scratch
Wenn die grüne Flagge angeklickt
verstecke dich
wechsle zu Kostüm (gesund v)
wiederhole (199) mal
  erzeuge Klon von (mir selbst v)
ende
wechsle zu Kostüm (infiziert v)
erzeuge Klon von (mir selbst v)
```

## Verhalten

Die Simulation der Personen muss sich je nach Zustand anders verhalten. Dazu definieren wir für jeden Zustand einen eigenen Block und erweitern den Simulationsschritt folgendermassen:

``` scratch
Definiere Simulationsschritt
gehe (3) er Schritt
pralle vom Rand ab
falls <(Kostüm (Name v)) = (gesund)>, dann
  Verhalten gesund :: custom
ende
falls <(Kostüm (Name v)) = (infiziert)>, dann
  Verhalten infiziert :: custom
ende
falls <(Kostüm (Name v)) = (krank)>, dann
  Verhalten krank :: custom
ende
falls <(Kostüm (Name v)) = (geheilt)>, dann
  Verhalten geheilt :: custom
ende
```

## Infektion

Eine Infektion findet statt, wenn eine gesunde Person Kontakt mit einer infizierten oder kranken Person hat.

``` scratch
Definiere Verhalten gesund
falls <<wird Farbe () berührt?> oder <wird Farbe () berührt?>>, dann
  wechsle zu Kostüm (infiziert v)
ende
```

## Wahrscheinlichkeit

In der Simulation haben wir Ereignisse, welche mit einer bestimmten Wahrscheinlichkeit eintreffen. Eine Wahrscheinlichkeit von 2% können wir in Scratch folgendermassen ausdrücken:

``` scratch
falls <(Zufallszahl von (0) bis (99)) < (2)?>, dann

ende
```

## Zeitliche Verzögerung

Um eine zeitliche Verzögerung zu modellieren, verwenden wir die eingebaute Stoppuhr. Wenn sich eine Person infiziert, so stellen wir einen Alarm:

``` scratch
setze [Alarm v] auf ((Stoppuhr) + 6)
```

``` scratch

```

[^1]: Quelle: [inf-schule.de, CC BY-SA-4.0](https://www.inf-schule.de/vernetzung/simulationen)

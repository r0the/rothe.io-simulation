# Material für Lehrer*innen
---

* [:pdf: Präsentation Monte Carlo-Simulation](./monte-carlo.pdf)
* [:pdf: Arbeitsblatt Monte Carlo-Simulation](./worksheet-monte-carlo.pdf)
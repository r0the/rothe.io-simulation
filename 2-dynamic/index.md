# 2 Dynamische Simulation

## Vensim

Mit Hilfe des Programms Vensim PLE wollen wir versuchen, Fliessprozesse zu simulieren und Voraussagen zu machen.
Das Programm ist eine vereinfachte Gratisversion eines kommerziellen Programms der Firma Ventana Systems, Inc.

* [:link: Vensim PLE-Download](https://gymkirchenfeld-my.sharepoint.com/:f:/g/personal/renato_nanni_gymkirchenfeld_ch/EjT8Y15P7NtOpeaOQUZcJc8BaFrRYqX6zISsFk5LWjjmyQ?e=ikVLsn)


## Konfiguration von Vensim

Vensim lässt sich besser bedienen, wenn die Icons in der Toolbar angeschrieben sind. Um dies einzuschalten, gehen Sie folgendermassen vor:

1. **Settings-Dialog öffnen**. Auf Windows wählen Sie dazu im Menü __Tools__ den Eintrag __Options__. Auf macOS wählen Sie dazu im Menü __Vensim PLE__ den Eintrag __Preferences__.

2. Wählen Sie die Kategorie __Toolbars__ und setzen Sie bei __Show icon labels__ ein Häkchen:

![](./vensim-icons.png)

3. Klicken Sie auf __Dismiss__. Wenn Sie den Dialog auf eine andere Weise schliessen, werden die Einstellungs nicht gespeichert.

# VenSim-Tutorial

::: exercise
#### :exercise: VenSim-Tutorial

1. Starten Sie VenSim auf ihrem Computer.
2. Starten Sie das folgende Lernvideo:

<v-video source="vimeo" id="54963645"></v-video>


Das Video dauert rund 23 Minuten und deckt die wichtigen Grundlagen in der Bedienung von VenSim ab. Da VenSim nicht intuitiv bedienbar ist, wird dringend empfohlen, dieses Tutorial 1:1 durchzugehen und das Modell, wie es gezeigt wird, zu erstellen.

**Hilfestellung:** Sie müssen zwei Fenster offen haben, sowohl das Tutorial, als auch Vensim. Sie können dazu mit den Tastenkombinationen [Windows]+[Left] und [Windows]+[Right] VenSim und den Browser auf je einer Bildschirmhälfte platzieren. Stoppen Sie das Video während dem Tutorial immer wieder, um die gezeigten Schritte selbst durchzuführen.
:::

### VenSim 9-Tutorial

<v-video source="youtube" id="hxPZ_H6bcqY"/>


::: exercise
#### :extra: Zusatzaufgaben

Sie sind durch und warten auf bessere Zeiten? Hier sind sie!

Hier finden Sie 12 weitere Beispiele zum Schmöckern:

* [:pdf: Dynamische Systeme auf verschiedenen Plattformen](http://rfdz.ph-noe.ac.at/fileadmin/Mathematik_Uploads/ACDCA/materialien/DynamischeSysteme_v1.pdf)

:::

---
Autor: Renato Nanni

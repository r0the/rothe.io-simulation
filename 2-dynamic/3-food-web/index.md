# Nahrungsnetze

Erstellen Sie mit Vensim ein stabiles Ökosystem «Wald» unter Berücksichtigung ihrer Vorkenntnisse:

- Erfassen Sie in Vensim das von BirdLife aufgestellte Nahrungsnetz mit allen 14 Organismen
- Benutzen Sie den Kopf: gibt es Verbindungen, die keinen Sinn machen? (Ein Mitarbeiter von BirdLife war wohl etwas übereifrig)
- Der Energiefluss ist gefragt (andere Faktoren wie pH, Jahreszeiten, etc werden nicht berücksichtigt)
- Alle Angaben sind in kJ
- Je Trophiebene gibt es einen Verlust von 90% (falls mehrere Beutetiere pro Jäger: die 90% «verteilen»!)

Erstellen Sie das System so, dass Prognosen gemacht werden können:

- Mit Schieberegler Menge anpassen (was passiert, wenn z.B. durch Biozideinsatz mehr Vögel sterben?)
- Erstellen Sie (im fertigen System den Einfluss eines neuen Organismus (z.B. ein Wolf, der auch Füchse frisst).

## Narungsnetz Wald

![](./food-web-wood.svg)

- **Buche:** Blätter, Holz und Früchte als Nahrung verschiedener Tiere
- **Gras:** Samen, Wurzeln und Blätter als Nahrung pflanzenfressender Säugetiere und Raupen
- **Brombeere:** Nektar und Beeren als Nahrung verschiedener Tiere
- **Veilchen:** Blätter als Nahrung für Raupen, Ameisen holen ölhaltige Früchte
- **Feldhase:** Ernährt sich von Grad und Kräutern, frisst im Winter Rinde von Sträuchern und jungen Bäumen
- **Waldmaus:** Frisst Samen und Früchte, Beeren, Insekten, Eier und Jungvögel von Bodenbrütern.
- **Maikäfer:** Frisst Blätter von Laubbäumen, Larven ernähren sich von Graswurzeln
- **Raupen:** Fressen Pflanzenteile (Gräser, Blätter, usw.)
- **Waldameise:** Frisst Spinnen, Insekten, Aas, zuckerhaltige Säfte, Samen
- **Spitzmaus:** Ernährt sich von Würmern, Insekten, Spinnen und Aas
- **Igel:** Frisst Insekten, Schnecken, Würmer, Eier und Junge von bodenbrütenden Vögeln
- **Rotkehlchen:** Frisst kleine Insekten, Larven, Würmer, Spinnen sowie Beeren von Sträuchern
- **Waldkauz:** Frisst Mäuse, Spitzmäuse, z.T. junge Hasen, Singvögel und grössere Käfer
---
Autor: Renato Nanni

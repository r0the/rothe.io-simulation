# Nakurusee

Der Nakurusee ist ein Ökosystem, das in den 70er-Jahren eine drastische Veränderung erfahren hat. Zu Beginn war das System stabil, dann wurde ein neuer Organismus in den See ausgesetzt. Als Folge kippte das System, wonach es wieder einen anderen stabilen Zustand erlangte.

![Flamingos am Nakurusee ©](./flamingos-nakuru.jpg)

## Fakten
- Liegt auf der ostafrikanischen Hochebene in Kenia
- Höhe: rund 2'000 m.ü.M.
- Fläche: 40 km^2
- Tiefe: max 4.5 m
- Kein Abfluss

## Zeitliche Entwicklung

### Vor 1972

Dieser See weist besondere Eigenschaften auf: Er ist flach und stark alkalisch mit einem pH-Wert von 10.5, so dass nur wenige Organismen in ihm existieren können.

Als Primärproduzent ist vor allem die Blaualge Spirulina vorhanden. Ihre Population ist so dicht, dass der See eine knallblaue Farbe aufweist und die Sichttiefe gerade mal 10 cm beträgt. Starke Winde durchmischen das Wasser, so dass trotz trüben Verhältnissen ein Grossteil der Algen Photosynthese betreiben kann. Der Zwergflamingo lebt von diesen Blaualgen und flitert sie mit seinem Schnabel heraus. Rund eine Million Zwergflamingos leben an diesem See. Sie entnehmen dem See täglich rund die Hälfte der Primärnettoproduktion der Algen (2.5g / m^2).

### Um 1972
Im Nakurusee wurde der Fisch «Tilapia» ausgesetzt mit dem Ziel, dass die Einwohner diesen fischen und sich von ihm ernähren können. Als Folge für das Ökosystem haben sich weitere Konsumenten am See niedergelassen: Pelikane, Adler, Reiher, sowie fischfressende Otter.

### 1974
Im Frühjahr gibt es plötzlich eine drastische Veränderung: Die Blaualgen verschwinden fast vollkommen und werden durch eine Grünalge ersetzt, der See wird klar und durchsichtig. Die Zwergflaminogs verschwinden ebenfalls, dafür breitet sich ein Ruderfusskrebschen aus, sowie der grosse Flamingo, der sich von den Ruderfusskrebschen ernähren kann.


::: exercise
#### :exercise: Aufgabe
- Lesen Sie die Hintergrundinformationen durch.
- Erfassen Sie in VenSim die Situation des ursprünglichen Zustandes vor 1972. (Was tun: Erstellen Sie eine Nahrungspyramide der Organismen. Was nicht tun: abiotische Faktoren wie Höhe oder pH wert können Sie getrost ignorieren).
- Ergänzen Sie nun um die zusätzlichen Faktoren (= neue Organismen), die in den nächsten Jahren aufgetaucht sind, bis das System 1974 wieder ein stabiles Gleichgewicht erlangte.
:::

---
Autor: Renato Nanni

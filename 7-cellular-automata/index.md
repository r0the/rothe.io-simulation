# 5 Zelluläre Automaten

**Zelluläre Automaten** dienen der Modellierung räumlich diskreter dynamischer Systeme. Räumlich diskret heisst, dass die Welt in Felder eingeteilt wird, wobei jedes Feld einen eigenen Zustand hat.
Die Entwicklung einzelner Felder zum Zeitpunkt $t + 1$ von den Zuständen der Felder in einer vorgegebenen Nachbarschaft und vom eigenen Zustand zum Zeitpunkt $t$ abhängt.

Man unterscheidet zwei verschiedene Arten von Nachbarschaft:

::: columns 2
![Von-Neumann-Nachbarschaft ©](./von-neumann-neighbourhood.svg)
***
![Moore-Nachbarschaft ©](./moore-neighbourhood.svg)
:::

Die Felder am Rande der Welt haben weniger Nachbarn, was die Simulation beeinflussen kann. Deshalb wird manchmal die Welt als Torus angesehen. Dann hat jede Zelle gleich viele Nachbarn.

![Torus ©](./torus.svg)


Zelluläre Automaten sind geeignet, das Verhalten komplexer natürlicher Systeme zu untersuchen. Beispiele:

-	biologisches Wachstum, Entstehung des Lebens
-	soziales, geologisches, ökologisches Verhalten
-	Verkehrsaufkommen und Verkehrsregelung
-	Entstehung und Entwicklung des Kosmos, von Galaxien und Sternen

# Game of Life

Das Game of Life ist ein Beispiel eines zellulären Automaten, der aus Gitterzellen besteht, die miteinander wechselwirken. Die folgenden Spielregeln wurden 1970 vom englischen Mathematikprofessor John Conway entwickelt:

## Regeln

Im Game of Life wird eine Art von Lebewesen simuliert. Jedes Feld hat zwei Zustände: es enthält ein Lebewesen oder nicht. Für die Berechnung der nächsten Zustands werden folgende zwei Regeln verwendet:

1. **Sterberegel:** Ist ein Lebewesen in einem Feld, so überlebt es, wenn genau **zwei oder drei** Nachbarfelder mit Lebenwesen besetzt sind, ansonsten stirbt es.
2. **Geburtsregel:** Ist ein Feld leer, so entsteht dort ein Lebewesen, wenn genau **drei** Nachbarfelder mit Lebewesen besetzt sind.

::: exercise
#### :exercise: Vergleich mit Räuber-Beute-Spiel
Vergleichen Sie das Game of Life mit dem Räuber-Beute-Spiel. Was sind die Gemeinsamkeiten, was die Unterschiede?
:::

<VueGameOfLife/>

::: exercise
#### :exercise: Experiment
Stellen Sie sich vor, sind eine Wissenschaftlerin oder ein Wissenschaftler und wollen die Welt der Game of Life untersuchen. Wenden Sie die wissenschaftliche Methode auf diese Welt an:

- Experimentieren Sie.
- Stellen Sie Theorien auf.
- Kategorisieren Sie die «Lebensformen».

:::


::: extra
#### :exercise: Python-Programm
- Kopiere den Python-Code in der Lösung nach Thonny und starte das Programm.
-	Verändere die Lebensregeln und beobachte das Resultat.

Für ambitionierte Programmierer: Programmiere das Game of Life selbst.

***
``` python ./game_of_life.py
```

:::

import random
import pgzrun

WEISS = (255, 255, 255)
ROT = (255, 0, 0)
SCHWARZ = (0, 0, 0)

TITLE = "Game of Life"
WIDTH = 600
HEIGHT = 600
SEITE = 50
welt = []
ZEITSCHRITT = 0.1

def zelle(x, y):
    if 0 <= x < SEITE and 0 <= y < SEITE:
        return welt[y * SEITE + x]
    else:
        return False


def erstelle_welt():
    for y in range(SEITE):
        for x in range(SEITE):
            welt.append(random.choice([False, True]))


def zeichne_welt():
    screen.fill(WEISS)
    s = WIDTH / SEITE
    for x in range(SEITE):
        for y in range(SEITE):
            rect = Rect(x * s, y * s, s, s)
            if zelle(x, y):
                screen.draw.filled_rect(rect, ROT)
            screen.draw.rect(rect, SCHWARZ)



def zaehle_nachbarn(x, y):
    anzahl = 0
    for yn in range(y - 1, y + 2):
        for xn in range(x - 1, x + 2):
            if (xn != x or yn != y) and zelle(xn, yn):
                anzahl = anzahl + 1
    return anzahl


def neuer_wert(x, y):
    nachbarn = zaehle_nachbarn(x, y)
    if zelle(x, y):
        return nachbarn == 2 or nachbarn == 3
    else:
        return nachbarn == 3


def simulationsschritt():
    global welt
    neue_welt = []
    for y in range(SEITE):
        for x in range(SEITE):
            neue_welt.append(neuer_wert(x, y))
    welt = neue_welt


def update():
    pass


def draw():
    zeichne_welt()

erstelle_welt()
clock.schedule_interval(simulationsschritt, ZEITSCHRITT)

pgzrun.go()

# :mdi-chart-bell-curve: Simulation

Eine *Simulation* oder Simulierung ist eine Vorgehensweise zur Analyse von Systemen, die für die theoretische oder formelmässige Behandlung zu komplex sind. Dies ist überwiegend bei *dynamischem Systemverhalten* gegeben. Bei der Simulation werden Experimente an einem Modell durchgeführt, um Erkenntnisse über das reale System zu gewinnen.

* [1 Grundlagen](?page=1-basics/)
* [2 Dynamische Simulation](?page=2-dynamic/)
* [3 Räuber-Beute-Beziehung](?page=3-predator-prey/)
* [4 Monte Carlo-Simulation](?page=4-monte-carlo/)
* [5 Multiagenten-Simulation](?page=5-multi-agent/)
* [6 Physikalische Simulation](?page=6-physics/)

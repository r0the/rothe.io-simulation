# Übungen

::: exercise
#### :exercise: Warten auf eine Sechs

Wie oft müssen Sie würfeln, bis Sie eine Sechs kriegen?

1. Stellen Sie eine Vermutung an.
2. Führen Sie eine Simulation durch.
:::

::: exercise
#### :exercise: Handyreparatur
Sie haben ein Handyreparatur-Geschäft und bieten an, dass Sie Handys innerhalb eines Tages reparieren. Sie wissen, dass Sie mit ungefähr 50 Reparaturen pro Woche rechnen müssen, die sich zufällig auf die sechs Tage Montag bis Samstag verteilen.

Ein Angestellter kann pro Tag 10 Reparaturen ausführen. Wie viele Angestellte brauchen Sie, damit Sie ihr Versprechen meistens halten können?

1. Stellen Sie eine Vermutung an.
2. Führen Sie eine Simulation durch. Bestimmen Sie mit einem Würfel für 50 Handys, an welchem Wochentag sie in die Reparatur gebracht werden. Machen Sie mehrere Versuchsreihen oder Vergleichen Sie mit den anderen Schüler*innen.
:::

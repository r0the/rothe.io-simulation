# 4.1 Näherung von π

Die Kreiszahl $\pi$ ist als das Verhältnis des Umfangs eines Kreises zu seinem Durchmesser definiert. $\pi$ ist eine irrationale Zahl und besitzt somit keine endliche Dezimaldarstellung.

## Monte-Carlo-Simulation

Die Näherung der Kreiszahl ist ein anschauliches Beispiel für eine Monte-Carlo-Simulation:

> Regentropfen fallen zufällig auf ein Quadrat. Zähle den Anteil der Regentropfen, welche wenige als die halbe Seitenlänge vom Mittelpunkt des Quadrats entfernt sind. Aus diesem Anteil lässt sich die Kreiszahl π annähern, umso genauer, je mehr Regentropfen fallen.




![Näherung der Kreiszahl π durch Zufall ©](./pi-monte-carlo.gif)

Mit einer Monte Carlo-Simulation kann die Kreiszahl $\pi$ durch Zufallszahlen berechnet werden. Es werden Regentropfen simuliert, welche auf ein Quadrat fallen. Für jeden Regentropfen werden also zwei zufällige Zahlen

$$ x \in [-1, 1], \qquad y \in [-1, 1] $$

bestimmt. Es werden alle Regentropfen $k$ gezählt, die innerhalb des Einheitskreises zu liegen kommen. Das Verhältnis von $k$ zu allen Regentropfen $n$ muss ungefähr dem Verhältnis der Kreisfläche ($\pi$) zur Fläche des Quadrats ($4$) entsprechen:

$$ \frac{k}{n} = \frac{\pi}{4} $$

Somit sollte das Vierfache des Verhältnisses von $k$ und $n$ der Kreiszahl $\pi$ entsprechen:

$$ 4\cdot\frac{k}{n} = \pi $$

::: exercise
#### ✏️ Aufgabe
Schreibe ein Programm, welches π mit der Monte-Carlo-Simulation annähert.
:::

## Weitere Verfahren

Es gibt verschiedene Verfahren, $\pi$ nummerisch anzunähern. Diese Verfahren sind **iterativ**. Das heisst, ein Berechnungsschritt wird mehrmals wiederholt, jede Wiederholung ergibt eine bessere Näherung. Ein Näherungsverfahren wird als besser betrachtet, wenn die Differenz des Näherungswertes zu $\pi$ schneller, das heisst nach weniger Iterationen klein wird, als bei einem anderen Verfahren.


### Summenformel von Leonhard Euler

π kann mit unendlichen Summenformeln angenähert werden. Beispielsweise ergibt die Summe der Kehrwerte der Quadratzahlen ein Sechstel vom Quadrat von π:

$$\frac{\pi^2}{6} = \frac{1}{1^2} + \frac{1}{2^2} + \frac{1}{3^2} + \frac{1}{4^2} + \ldots$$

### Produktformel von Leonhard Euler

In seinem Werk *Einleitung in die Analysis des Unendlichen* stellt Leonard Euler folgende Produktformel für Primzahlen auf:

$$\frac{\pi^2}{6} = \frac{2^2}{2^2-1}\cdot \frac{3^2}{3^2-1}\cdot \frac{5^2}{5^2-1}\cdot \frac{7^2}{7^2-1}\cdot\ldots$$

Jedes Primzahlquadrat wird durch die um eins kleinere Zahl dividiert. Werden diese Brüche alle multipliziert, so ergibt sich ein Sechstel vom Quadrat von π.


### Reihe von Bailey, Borwein und Plouffe

Die Mathematiker Bailey, Borwein und Plouffe haben 1995 eine Summenformel zur Berechnung von π veröffentlicht. Ein Glied der Reihe ist so definiert:

$$a_k = \frac{1}{16^k} \cdot \left( \frac{4}{8k+1} - \frac{2}{8k+4} - \frac{1}{8k+5} - \frac{1}{8k+6} \right)$$

Die Summe der Reihe ergibt π:

$$\pi = a_0 + a_1 + a_2 + a_3 + \ldots$$

::: exercise
#### ✏️ Aufgabe
- Erweitere das Programm so, dass π mit jedem der obenstehenden Näherungsverfahren berechnet wird.
- Das Programm soll für bestimmte Anzahl Iterationen die Differenz der Näherung zur in der Programmiersprache definierten π-Konstante ausgeben.

Beantworte folgende Fragen:
- Wie viele Iterationen sind bei jedem Verfahren nötig, bis in der Programmiersprache keine Differenz zur Konstante mehr erkennbar ist?
- Welches Verfahren ist das beste, welches das schlechteste?
:::

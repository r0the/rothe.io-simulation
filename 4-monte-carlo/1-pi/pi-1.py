import random
import math

def pi_monte_carlo(n):
    k = 0
    for i in range(n):
        x = random.uniform(-1, 1)
        y = random.uniform(-1, 1)
        if x**2 + y**2 < 1:
            k = k + 1
    return 4 * k / n


def pi_euler_summe(n):
    s = 0
    for i in range(n):
        s = s + 1 / (i + 1) ** 2
    return (s * 6) ** 0.5

n = 10
print("n\tMonte Carlo\tEulersche Summenformel")
while n < 10**10:
#    print(n, "Durchgänge")
    p1 = pi_monte_carlo(n)
#    print("Monte Carlo:", p1 - math.pi)
    p2 = pi_euler_summe(n)
#    print("Euler Summenformel:", p2 - math.pi)
    print(str(n) + "\t" + str(p1) + "\t" + str(p2))

    n = n * 10

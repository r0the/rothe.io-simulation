# 4 Monte-Carlo-Simulation[^1]

Eine **Monte-Carlo-Simulation** ist ein Verfahren, bei dem eine sehr grosse Zahl gleichartiger Zufallsexperimente die Basis darstellt. Es wird dabei versucht, analytisch nicht oder nur aufwendig lösbare Probleme mit Hilfe der Wahrscheinlichkeitstheorie numerisch zu lösen. Als Grundlage ist vor allem das Gesetz der großen Zahlen zu sehen. Die Zufallsexperimente können real beispielsweise durch Würfeln durchgeführt werden. Im Computer werden zufällige Ereignisse durch sogenannte **Pseudozufallszahlen** simuliert. Dazu werden Algorithmen eingesetzt, welche scheinbar zufällige Zahlen berechnen.

Diese Methode wurde erstmals 1946 verwendet, um die Neutronendiffusion in nuklearen Materialien zu simulieren. Es ging um die geheime Entwicklung der ersten Atombombe am Los Alamos Scientific Laboratory. Daher musste sogar für diese mathematische Methode ein Codename verwendet werden. Der Name entstand in Anspielung auf den Onkel eines Mitarbeiters, welcher sich für Glücksspiele immer Geld von Verwandten geliehen hatte.

## Anwendungen

Als Alternative zur analytischen Lösung von Problemen rein mathematischer Herkunft:
- die Näherung der Kreiszahl π
- die Berechnung der Fläche unter einer Funktion (bestimmtes Integral)

Die Nachbildung von komplexen Prozessen, die nicht direkt analysiert werden können:
- Produktionsprozesse in einem Fertigungsunternehmen
- Wetter und Klima der Erde
- Bildgebungsverfahren in der Nuklearmedizin
- Risikomanagement in Unternehmen
- räumliche Verteilung des Neutronenflusses (Kernfusion)

## Inhalt

* [4.1 Näherung von Pi](?page=1-pi/)
* [4.2 Monty Hall-Problem](?page=2-monty-hall/)

[^1]: Quelle: [Wikipedia – Monte-Carlo-Simulation](https://de.wikipedia.org/wiki/Monte-Carlo-Simulation)
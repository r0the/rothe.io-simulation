# 4.2 Monty Hall-Problem

Monty Hall war ein kanadischer Showmaster. Er hatte die Spielshow *Let's Make a Deal* moderiert, bei welcher hinter einer von drei Türen ein Hauptpreis zu finden war. Hinter den anderen zwei Türen befand sich als Symbol für den Misserfolg eine Ziege.

![Hinter einer Türe ist eine Ziege ©](./goat-and-door.svg)

Nachdem die Kandidatin eine Türe gewählt hatte, öffnete der Moderator eine andere Türe mit einer Ziege. Danach hatte die Kandidatin die Möglichkeit, ihre Wahl zu ändern. Die Frage ist, ob es die Gewinnchance erhöht, das Tor zu wechseln.

1990 wurde die Frage bekannt und zum Gegenstand einer kontroversen Debatte, als Marilyn vos Savants in Ihrer Kolumne *Ask Marilyn* im Magazin *Parade* eine Lösung für das Problem vorgeschlagen hat. Sie empfahl dem fragenden Leserbriefschreiber das Tor zu wechseln, da dies zu einer Gewinnchance von zwei Dritteln führt.


::: columns 2
![Monty Hall ©](./monty-hall.jpg)
***
![Marylin vos Savant ©](./marylin-vos-savant.jpg)
:::

[^1]: Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Ziegenproblem)

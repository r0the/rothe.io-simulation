# Brettspiel

## Material

- Spielfeld (6×6 Felder)
- zwei verschiedenfarbige Spielwürfel
- 36 doppelseitige Spielsteine (wechselseitig Blattläuse und Marienkäfer)

## Erklärung

Das Räuber-Beute-Spiel ist eine Brettspiel für zwei Spieler*innen. Es wird eine einfache Räuber-Beute-Beziehung simuliert. Das Spielfeld symbolisiert einen Garten, in welchem Blattläuse und Marienkäfer leben, sich vermehren, gefressen werden und verhungern. Der eine Spieler spielt Blattläuse (Beute), die andere Spielerin spielt Marienkäfer (Räuber).

## Aufstellung

Das Spielfeld wird zufälligt mit mindestens 16 Spielsteinen belegt, welche zufällig mit der Marienkäfer- oder der Blattlaus-Seite nach oben liegen. Auf jedem Feld darf höchstens ein Spielstein liegen.

## Ablauf

Die Spieler*innen ziehen abwechselnd. Ein Zug besteht immer aus der Ermittlung eines Feldes und Anwendung der entsprechenden Regeln auf dieses Feld. Das Feld wird zufällig durch Erwürfeln der Koordinaten bestimmt. Je nachdem, wer am Zug ist, werden andere Regeln angewendet:

### Marienkäfer

- Falls auf dem Feld eine Blattlaus ist, wird der Spielstein durch umdrehen in einen Marienkäfer verwandelt. *Bedeutung: Der Räuber vermehrt sich durch das Aufbrauchen von Nahrung.*
- Ansonsten muss der Spieler einen beliebigen Marienkäfer-Spielstein vom Spielfeld entfernen. *Bedeutung: Die Räuber-Population nimmt ab, da keine Nahrung vorhanden ist.*

### Blattlaus

- Falls auf dem Feld ein Marienkäfer ist, so passiert nichts.
- Ansonsten platziert die Spielerin einen Blattlaus-Stein auf dem Feld oder einem freien angrenzenden Feld.

### Protokoll

Es wird Protokoll geführt. Immer nach 10 Zügen wird die Anzahl Marienkäfer und Blattläuse bestimmt und festgehalten.

## Schluss

Nach 200 Zügen ist das Spiel beendet. Die Entwicklung der Populationen wird grafisch dargestellt.

## Einsatz von Insektizid

Als Zusatzregel spielt eine Gärtnerin mit. Sie würfelt zehn Runden lang jede Runde sechs Mal und entfernt jeden Spielstein auf den ermittelten Feldern.

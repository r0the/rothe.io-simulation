from guizero import *
import random

BREITE = 90
HOEHE = 60
ANZAHL_FISCHE = 240
ANZAHL_HAIE = 40

FENSTER_X = 1200
FENSTER_Y = 800
GRAPH_HOEHE = 100

# Tierarten
TOT = -2
FISCH = 0
HAI = 1
WASSER = 2

ENERGIE_FISCH = 2

FARBE = [
    ( 34, 168, 109), # Fisch
    (233, 110,  68), # Hai
    ( 47,  47,  49)  # Wasser
]

def welt_index(x, y):
    return (x % BREITE) + (y % HOEHE) * BREITE

class Welt:
    def __init__(self):
        self.neu()

    def neu(self):
        self.tiere = []
        self.welt = [None] * (BREITE * HOEHE)
        self.anzahl = [0, 0]
        self.anzahl_alt = [0, 0]
        self.reproduktionsalter = [4, 24]
        self.anfangsenergie = [2, 9]
        self.zeit = 0
        for i in range(ANZAHL_FISCHE):
            self.neues_tier_zufall(FISCH)
        for i in range(ANZAHL_HAIE):
            self.neues_tier_zufall(HAI)

    def schritt(self):
        self.zeit = self.zeit + 1
        alle_tiere = [tier for tier in self.tiere]
        for tier in alle_tiere:
            tier.schritt()

    def tier_bei(self, x, y):
        resultat = self.welt[welt_index(x, y)]
        if resultat:
            return resultat
        else:
            return Wasser(x, y)

    def nachbarfelder(self, x, y):
        return [
            self.tier_bei(x - 1, y),
            self.tier_bei(x + 1, y),
            self.tier_bei(x, y - 1),
            self.tier_bei(x, y + 1)
        ]

    def setze_tier(self, tier):
        self.welt[welt_index(tier.x, tier.y)] = tier

    def entferne_tier(self, tier):
        self.welt[welt_index(tier.x, tier.y)] = None

    def toete_tier(self, tier):
        self.entferne_tier(tier)
        self.tiere.remove(tier)
        self.anzahl[tier.art] = self.anzahl[tier.art] - 1
        tier.art = TOT

    def neues_tier(self, art, x, y, alter, energie):
        tier = Tier(self, art, x, y, alter, energie)
        self.tiere.append(tier)
        self.setze_tier(tier)
        self.anzahl[tier.art] = self.anzahl[tier.art] + 1

    def neues_tier_zufall(self, art):
        leeres_feld = False
        while not leeres_feld:
            x = random.randrange(BREITE)
            y = random.randrange(HOEHE)
            if self.tier_bei(x, y).art == WASSER:
                leeres_feld = True
        alter = random.randrange(self.reproduktionsalter[art])
        energie = self.anfangsenergie[art]
        self.neues_tier(art, x, y, 0, energie)

class Tier:
    def __init__(self, welt, art, x, y, alter, energie):
        self.welt = welt
        self.x = x
        self.y = y
        self.art = art
        self.alter = alter
        self.energie = energie

    def bewege(self, x, y):
        if self.alter >= self.welt.reproduktionsalter[self.art]:
            # Reproduktion, ein neues Tier entsteht an der neuen Position
            self.alter = 0
            if self.art == HAI:
                self.energie = self.energie / 2
            self.welt.neues_tier(self.art, x, y, 0, self.energie)
        else:
            self.welt.entferne_tier(self)
            self.x = x
            self.y = y
            self.welt.setze_tier(self)

    def schritt(self):
        # ein totes Tier macht nichts
        if self.art == TOT:
            return
        if self.art == HAI:
            self.energie = self.energie - 1
        self.alter = self.alter + 1
        nachbarn = self.welt.nachbarfelder(self.x, self.y)
        if self.art == HAI:
            # suche benachbarte Fische
            fische = []
            for nachbar in nachbarn:
                if nachbar.art == FISCH:
                    fische.append(nachbar)
            if len(fische) > 0:
                futter = random.choice(fische)
                self.energie = self.energie + futter.energie
                self.welt.toete_tier(futter)
                self.bewege(futter.x, futter.y)
                return
            # ein Hai stirbt, wenn er keine Energie mehr hat
            if self.energie <= 0:
                self.welt.toete_tier(self)
                return
        leere_felder = []
        for nachbar in nachbarn:
            if nachbar.art == WASSER:
                leere_felder.append(nachbar)
        if len(leere_felder) > 0:
            ziel = random.choice(leere_felder)
            self.bewege(ziel.x, ziel.y)

class Wasser:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.art = WASSER

def graph_y(anzahl):
    return GRAPH_HOEHE - anzahl * 0.9 * GRAPH_HOEHE / (BREITE * HOEHE)

welt = Welt()

def zeichne_welt():
    for x in range(BREITE):
        for y in range(HOEHE):
            tier = welt.tier_bei(x, y)
            gitter.set_pixel(x, y, FARBE[tier.art])

def zeichne_graph():
    for art in [FISCH, HAI]:
        scale = 1 if art == FISCH else 5
        graph.line(
            welt.zeit,
            graph_y(scale * welt.anzahl_alt[art]),
            welt.zeit + 1,
            graph_y(scale * welt.anzahl[art]),
            color=FARBE[art], width=3)
        welt.anzahl_alt[art] = welt.anzahl[art]

def simulationsschritt():
    welt.reproduktionsalter[FISCH] = alter_fische.value
    welt.reproduktionsalter[HAI] = alter_haie.value
    welt.anfangsenergie[FISCH] = energie_fische.value
    welt.anfangsenergie[HAI] = energie_haie.value
    welt.schritt()
    zeichne_welt()
    zeichne_graph()

def neu():
    welt.neu()
    zeichne_welt()
    graph.clear()

def setze_geschwindigkeit():
    app.cancel(simulationsschritt)
    dt = 2200 - geschwindigkeit.value * 200
    if dt < 2200:
        app.repeat(dt, simulationsschritt)

app = App(width=FENSTER_X, height=FENSTER_Y)
simulation = Box(app, width="fill", align="top")
graph = Drawing(app, width="fill", align="bottom", height=GRAPH_HOEHE)
graph.bg = (220, 220, 220)
gitter = Waffle(simulation, height=HOEHE, width=BREITE, align="right", pad=1, dim=9)
steuerung = Box(simulation, height="fill", align="left", layout="grid")

# Steuerung
neu_taste = PushButton(steuerung, text="Zurücksetzen", grid=[1, 0], command=neu)
Text(steuerung, text="Geschwindigkeit", grid=[0, 1], align="bottom")
geschwindigkeit = Slider(steuerung, grid=[1, 1], start=0, end=10, command=setze_geschwindigkeit)
Text(steuerung, text="Reproduktionsalter Fische", grid=[0, 2], align="bottom")
alter_fische = Slider(steuerung, grid=[1, 2], start=1, end=40)
Text(steuerung, text="Reproduktionsalter Haie", grid=[0, 3], align="bottom")
alter_haie = Slider(steuerung, grid=[1, 3], start=1, end=40)
Text(steuerung, text="Energie Fische", grid=[0, 4], align="bottom")
energie_fische = Slider(steuerung, grid=[1, 4], start=1, end=20)
Text(steuerung, text="Energie Haie", grid=[0, 5], align="bottom")
energie_haie = Slider(steuerung, grid=[1, 5], start=1, end=20)

alter_fische.value = 4
alter_haie.value = 24
energie_fische.value = 2
energie_haie.value = 9

neu()
app.display()

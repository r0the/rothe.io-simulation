# 3.4 Multiagenten-Simulation

**Wa Tor** ist eine diskrete Räuber-Beute-Simulation, welche von Alexander Keewatin Dewdney 1984 veröffentlich wurde.

## Welt

Die Wa Tor-Welt besteht aus quadratischen Feldern, welche zu einem Torus geformt werden. Dies ist der Einfachheit halber gewählt. Eine solche Welt ist viel einfacher zu simulieren, da keine speziellen Regeln für die Ränder beachtet werden müssen.

![Torus ©](./torus.svg)

## Lebewesen

Die Wa Tor-Welt ist mit Fischen und Haien bevölkert. Fische ernähren sich von Plankton, was in der Simulation nicht betrachtet wird. Die Haie fressen die Fische. Auf jedem Feld der Welt kann sich zu jedem Zeitpunkt nur ein Lebewesen befinden, also ein Fisch oder ein Hai.

## Regeln für die Fische

- Jeder Fisch bewegt sich zufällig auf eines der vier angrenzenden Felder weiter, sofern eines davon leer ist.
- Wenn ein Fisch ein bestimmtes Alter erreicht, so wird auf dem ursprünglichen Feld ein neuer Fisch geboren. Das Alter des Fischs wird zurückgesetzt.

## Regeln für die Haie

- Ein Hai bewegt sich zufällig auf ein angrenzendes Feld, auf welchem sich ein Fisch befindet. Er frisst diesen Fisch. Damit wird seine Energie um diejenige des Fischs erhöht.
- Ist kein Fisch auf einem angrenzenden Feld vorhanden, so bewegt sich der Hai zufällig auf ein angrenzendes leeres Feld.
- Wenn ein Hai ein bestimmtes Alter erreicht, so wird auf dem ursprünglichen Feld ein neuer Hai geboren. Das Alter des Hais wird zurückgesetzt. Der ursprüngliche Hai gibt die Hälfte seiner Energie an den neuen Hai weiter.
- In jedem Simulationsschritt verliert der Hai Energie.
- Wenn der Hai keine Energie mehr hat, stirbt er.

## Python-Programm

Das folgende Python-Programm simuliert die Wa Tor-Welt:

``` python wator.py
```

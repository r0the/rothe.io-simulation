# Differenzialgleichungen

Die Lotka-Volterra-Gleichungen sind ein System zweier Differentialgleichungen erster Ordnung. Sie beschreiben die erste Lotka-Volterra-Regel mathematisch.

## Allgemeine Populationsgleichung

Eine Population verändert sich durch die Reproduktion und das Sterben der Individuen. Dies kann als Ableitung der Population nach der Zeit ausgedrückt werden:

$$ \dot{f}(P) = \frac{\text{d}P}{\text{d}t} = P (r - s)$$

Dabei ist $P$ die Anzahl Individuen der Population, $r$ ist die Reproduktionsrate und $s$ ist die Sterberate. Mit 

## Lotka-Volterra-Gleichungen

Die Lotka-Volterra-Gleichungen stellen eine Räuber- und eine Beutepopulation in Bezug:

$$ \frac{\text{d} B}{\text{d} t} = B (r_B - s_B\cdot R) \qquad\qquad \frac{\text{d} R}{\text{d} t} = R(r_R\cdot B - s_R) $$

Dabei haben die Symbole folgende Bedeutung:

| Symbol | Bedeutung                            |
|:------ |:------------------------------------ |
| $B$    | Anzahl der Beutelebewesen            |
| $r_B$  | Reproduktionsrate der Beutelebewesen |
| $s_B$  | Sterberate der Beutelebewesen        |
| $R$    | Anzahl der Räuber                    |
| $s_R$  | Sterberate der Räuber                |
| $r_R$  | Reproduktionsrate der Räuber         |

Es kann festgestellt werden, dass die Sterberate der Beutelebewesen abhängig ist von der Anzahl Räuber. Die Reproduktionsrate der Räuber ist abhängig von der Anzahl Beutelebewesen.

## Mathematische Lösung

Solche Differentialgleichungssysteme können mit Methoden des Mathematik analysiert werden.

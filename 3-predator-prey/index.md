# 3 Räuber-Beute-Beziehung

Mit **Räuber-Beute-Beziehung** wird die dynamische Wechselwirkung zwischen einer Räuber- und einer Beutepopulation über längere Zeiträume beschrieben. Räuber-Beute-Beziehungen stellen einen Ausschnitt einer Nahrungskette dar[^1].

## Simulationsarten

Eine Räuber-Beute-Beziehung kann mit den Gleichungen von Lotka und Volterra als statische oder dynamische Simulation dargestellt werden.

* [3.1 Lotka-Volterra](?page=1-lotka-volterra/)
* [3.2 Differentialgleichungen](?page=2-differential-equations/)
* [3.3 Nummerische Näherung](?page=3-nummeric-approximation/)
* [3.4 Multiagenten-Simulation](?page=4-wa-tor/)
* [3.5 Brettspiel](?page=5-board-game/)

[^1]: Quelle: [Wikipedia – Räuber-Beute-Beziehung](https://de.wikipedia.org/wiki/Räuber-Beute-Beziehung)
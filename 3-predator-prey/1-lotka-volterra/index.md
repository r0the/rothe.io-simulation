# Lotka-Volterra-Regeln[^1]

Die Lotka-Volterra-Regeln, auch Lotka-Volterra-Gesetze oder nur Volterra-Regeln genannt, umfassen drei Regeln zur quantitativen Beschreibung der Populationsdynamik in Räuber-Beute-Beziehungen.

Die den drei Regeln zugrundeliegenden mathematischen Lotka-Volterra-Gleichungen wurden 1925 und 1926 unabhängig voneinander von dem österreichisch-amerikanischen Chemiker Alfred J. Lotka und dem italienischen Mathematiker und Physiker Vito Volterra formuliert.

## Periodische Populationsschwankung

Die Populationsgrößen von Räuber und Beute schwanken periodisch. Dabei folgen die Schwankungen der Räuberpopulation phasenverzögert denen der Beutepopulation. Die Länge der Perioden hängt von den Anfangsbedingungen und von den Wachstumsraten der Populationen ab.

## Konstanz der Mittelwerte

Die über genügend lange Zeiträume gemittelten Größen (Mittelwert) der Räuber- bzw. Beutepopulation sind konstant. Die Grösse der Mittelwerte hängt nur von den Wachstums- und Rückgangsraten der Populationen, nicht aber von den Anfangsbedingungen ab.

## Störung der Mittelwerte

Werden Räuber- und Beutepopulation gleichermaßen proportional zu ihrer Grösse dezimiert, so vergrößert sich kurzfristig der Mittelwert der Beutepopulation, während der Mittelwert der Räuberpopulation kurzfristig sinkt.

## Hudson Bay Company

Als Lehrbuchbeispiel für die Erste und Zweite Volterra-Regel gelten die Fangaufzeichnungen der Hudson’s Bay Company, die über 90 Jahre lang geführt wurden. Danach schwankten der Eingang von Fellen von Luchsen (Räuber) und Schneeschuhhasen (Beute) mit einer Periode von 9.6 Jahren.

Zwar schwankten die Zahlen der jährlich abgelieferten Felle bei Luchsen zwischen 1'000 und 70'000 sowie bei den Schneeschuhhasen zwischen 2000 und 160'000, die Mittelwerte bei Betrachtung mehrerer Perioden liegen jedoch bei ca. 20'000 (Luchse) und 80'000 (Schneeschuhhasen).

Hier ist eine Excel-Datei mit den Rohdaten:

* [:xlsx: lynxhare.xlsx](./lynxhare.xlsx)

(Quelle: [GitHub](https://github.com/teojcryan/ma930-lotka-volterra))

[^1]: Quelle: [Wikipedia – Lotka-Volterra-Regeln](https://de.wikipedia.org/wiki/Lotka-Volterra-Regeln)

# 3.3 Nummerische Näherung

## Allgemeine Populationsgleichung

Eine Population verändert sich durch die Reproduktion und das Sterben der Individuen. Dies kann als Gleichung so ausgedrückt werden.

$$ \frac{\Delta P}{\Delta t} = P (r - s)$$

Dabei ist $P$ die Anzahl Individuen der Population, $r$ ist die Reproduktionsrate und $s$ ist die Sterberate. $\Delta P$ ist die Änderung der Anzahl Individuen pro Zeiteinheit $\Delta t$.

## Lotka-Volterra-Gleichungen

Die Lotka-Volterra-Gleichungen stellen eine Räuber- und eine Beutepopulation in Bezug:

$$ \frac{\Delta B}{\Delta t} = B (r_B - s_B\cdot R) \qquad\qquad \frac{\Delta R}{\Delta t} = R(r_R\cdot B - s_R) $$

Dabei haben die Symbole folgende Bedeutung:

| Symbol | Bedeutung                            |
|:------ |:------------------------------------ |
| $B$    | Anzahl der Beutelebewesen            |
| $r_B$  | Reproduktionsrate der Beutelebewesen |
| $s_B$  | Sterberate der Beutelebewesen        |
| $R$    | Anzahl der Räuber                    |
| $s_R$  | Sterberate der Räuber                |
| $r_R$  | Reproduktionsrate der Räuber         |

Es kann festgestellt werden, dass die Sterberate der Beutelebewesen abhängig ist von der Anzahl Räuber. Die Reproduktionsrate der Räuber ist abhängig von der Anzahl Beutelebewesen.

## Numerische Näherung mit Tabellenkalkulation

In einem Tabellenkalkulationsprogramm kann eine numerische Näherung berechnet werden.
Dazu wird eine Tabelle konstruiert, welche die Zustände des Modells zu den Zeitpunkten $t=0,1,3,\ldots$ abbildet:

| Zeit | Räuber     | Beute      | ΔR                      | ΔB                       |
|:---- |:---------- |:---------- |:----------------------- |:------------------------ |
| $0$  | $R_0$      | $B_0$      | $R_0(r_R\cdot B_0-s_R)$ | $B_0(r_B -s_B\cdot R_0)$ |
| $1$  | $R_0+ΔR_0$ | $B_0+ΔB_0$ | $R_1(r_R\cdot B_1-s_R)$ | $B_1(r_B -s_B\cdot R_1)$ |


![](./lotka-volterra.png)
# Modellieren[^1]

Modelle veranschaulichen Theorien, indem sie Teile der Realität imitieren. Zum Prozess der Modellierung gehört nicht nur das Imitieren, sondern vor allem auch die Festlegung des abgebildeten Realitätsausschnitts.

In vielerlei Hinsicht bedeutet Modell dasselbe wie Theorie – beide entwickeln ein Bild (eines Ausschnitts) der Realität, mit dem Zweck, diesen Wirklichkeitsausschnitt möglichst explizit zu beschreiben bzw. zu erklären. Ein Modell ist also eine Theorie in einer besonders anschaulichen Form.

## Modellierung
Bevor man eine Simulation durchführen kann, muss man ein funktionales Computermodell entwickeln, also den relevanten Wirklichkeitsausschnitt modellieren. Das heisst, die Komplexität der Realität wird auf die wesentlichen Faktoren reduziert. Die Realität wird also nicht vollständig dargestellt. Diese Vollständigkeit ist aber gar nicht beabsichtigt, im Gegenteil: Es sollen lediglich die wesentlichen Einflussfaktoren identifiziert und dargestellt werden, die für den realen Prozess bedeutsam sind.

Der Prozess der Modellierung lässt sich grob in zwei Schritte unterteilen:

1. **Abstrahieren/Reduzieren:** Der genaue Geltungsbereich bzw. Modellkontext muss definiert werden und die wesentlichen Einflussfaktoren bestimmt. Das Ziel ist ein möglichst einfaches Modell, das aber trotzdem die relevantesten Faktoren berücksichtigt.
2. **Konkretisieren/Umsetzen:** Der Erklärungsansatz muss in ein funktionales Modell überführt werden, es gilt also, alle relevanten Eigenschaften und Funktionsweisen des reduzierten Abbilds als Programmcode – und damit maximal explizit – zu formulieren, genauer: als (auf verschiedene Klassen verteilte) Variablen und Methoden. Damit die Simulation bzw. das virtuelle Experiment dann konkret durchgespielt werden kann, müssen häufig auch relevante Aspekte der Umwelt bzw. des experimentellen Settings abgebildet, also modelliert werden.

![](./science-simulation-2.png)

Die wissenschaftliche Methode läuft auf eine kontinuierliche Weiterentwicklung von Theorien hinaus – das gilt natürlich auch für Modelle. Das Verhalten des Modells wird also mit dem Verhalten der Realität (=Evidenz) abgeglichen. In der Folge wird das Modell angepasst – also ein weiterer Durchlauf durch die obigen Modellierungsschritte vorgenommen – , so dass es im Laufe der Zeit zu einem immer besseren Abbild des relevanten Realitätsausschnitts wird und immer verlässlichere Vorhersagen über die Realität machen kann. Der Wert eines Modells beruht darauf, möglichst anschaulich und einfach zu sein – aus diesen Eigenschaften ergibt sich aber nur dann wissenschaftliche Erkenntnis, wenn aus dem Modell Aussagen über die abgebildete Realität abgeleitet werden können.

[^1]: Quelle: [oinf.ch](https://oinf.ch/kurs/simulationen/modellieren/), CC BY-NC-SA 4.0

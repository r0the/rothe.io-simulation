# 1 Grundlagen[^1]

Simulationen haben sich als drittes Standbein des wissenschaftlichen Erkenntnisgewinns etabliert; die Informatik fungiert daher in einer Vielzahl von Fachgebieten als Hilfswissenschaft.

## Die Wissenschaftliche Methode
Schon immer haben Menschen versucht, sich die Welt zu erklären – aus reiner Neugier, oder um ihr Verständnis natürlicher Phänomene zum eigenen Vorteil einzusetzen. Konkret bedeutet das, man entwickelt eine Theorie, also eine mögliche Erklärung für ein Phänomen, mit dem man sich in seiner Lebenswelt konfrontiert sieht. Anschliessend versucht man, die Theorie anhand von weiterer Evidenz – also konkreten Beobachtungen – mit der Wirklichkeit abzugleichen.
Seit dem Mittelalter hat sich aus diesem grundlegenden Mechanismus das System der Wissenschaft entwickelt. Dabei ist der Kern, dass gewisse Ansprüche an wissenschaftliche Theorien und auch an wissenschaftliche Evidenz gestellt werden.

Das Ziel des wissenschaftlichen Systems ist es, mithilfe dieser Ansprüche an Theorie und Evidenz eine gemeinsame Basis zu schaffen, auf der man Erklärungsansätze vergleichen kann. In der modernen Wissenschaft forciert man diesen Konkurrenzkampf der Theorien, indem man insbesondere nach Situationen sucht, für die verschiedenen Theorien unterschiedliche Vorhersagen machen. Dann versucht man ebenjene Situation nachzustellen, beispielsweise in Form eines gezielten Experiments. Je nach Ausgang des Experiments wird dann zumindest eine der Theorien verworfen oder so angepasst, dass sie auch mit der neuen Evidenz vereinbar wird. Auf diese Weise konkurrieren verschiedene Erklärungsansätze oft über längere Zeit miteinander. Dafür, dass sich im Laufe der Zeit doch bestimmte Theorien durchsetzen, sind insbesondere zwei Qualitätsmerkmale verantwortlich:

**Einfachheit:** Wenn verschiedene Theorien ähnlich gut zur vorhandenen Evidenz passen, dann gilt die einfachere Erklärung als die bessere. Dieses Prinzip ist bekannt unter dem Namen [Ockhams Rasiermesser][1], bzw. Sparsamkeitsprinzip.

**Gegenstandsbereich:** Wenn zwei Theorien bestimmte Phänomene – bzw. die zugehörige Evidenz – ähnlich gut erklären, eine davon aber noch auf weitere Phänomene anwendbar ist, dann gilt die allgemeinere Erklärung als die bessere.

Wissenschaftler tun also eigentlich zwei Dinge: Zum einen erarbeiten und verbessern sie Theorien/Modelle, zum anderen versuchen sie, durch Beobachtungen, Messungen und Experimente neue Evidenz zu schaffen, um die Theorien zu be- oder widerlegen. Und weil das alles im Detail nicht ganz einfach ist, verbringen sie ausserdem viel Zeit damit, sich darüber zu streiten, welche Evidenz verlässlicher und welche Theorie einfacher oder allgemeiner ist. Auf die Dauer jedoch – das ist das Essentielle an der wissenschaftlichen Methode – setzen sich die besseren Erklärungen durch, weil immer deutlicher wird, dass die aus der Theorie abgeleiteten Vorhersagen durch immer neue Evidenz bestätigt werden. Die rasanten technischen Entwicklungen der vergangenen 150 Jahre bezeugen den Erfolg der wissenschaftlichen Methode – insbesondere was naturwissenschaftliche Forschungsgebiete angeht.

![](./scientific-method.png)

## Simulation als drittes Standbein der Wissenschaft

Und was hat das alles mit Informatik zu tun? Man kann behaupten, dass die Informatik eine neue Methode zur Verfügung stellt, mit deren Hilfe sich der wissenschaftliche Erkenntnisgewinn in den letzten Jahrzehnten noch einmal rasant beschleunigt hat: die Simulation.

Wie wichtig Simulationen in der Wissenschaft inzwischen sind, zeigt sich u.a. daran, dass sich in den meisten Fachgebieten – nicht nur in den Naturwissenschaften – eigene Unterbereiche mit Lehrstühlen und Ausbildungsgängen im Bereich Computational XY etabliert haben (z.B. Computational Physics, Computational Biology, Computational Medicine, Computational Economics, Computational Psychology, Computational Anthropology, Computational Linguistics). Gemeinsam haben diese Bereiche, dass es zumeist um die Erforschung von Systemen geht, in denen sich das Gesamtbild aus der Interaktion vieler Einzelteile ergibt. Weil diese Interaktionen selbst mit verhältnismässig einfachen Komponenten schnell komplex werden, gibt es oftmals gar keine andere Möglichkeit, als das System numerisch zu simulieren (und weil verschiedene solcher Systeme erstaunlich viele Gemeinsamkeiten aufweisen, sind Komplexe Systeme selbst inzwischen ein eigenständiges Forschungsgebiet).

![](./science-simulation-1.png)

## Wozu simulieren?
> «Dass die Entwicklung und der Einsatz von Simulationsverfahren bereits heute mit Fug und Recht als große Herausforderung für Wissenschaft und Wirtschaft bezeichnet werden können und in Zukunft noch stark an Bedeutung zulegen werden, kann schwerlich bestritten werden. Zu offenkundig ist, dass das klassische Instrumentarium – also theoretische Analyse und Experiment – auf sich alleine gestellt immer öfter an seine Grenzen stößt. So führt die Erfordernis einer möglichst realitätsnahen Beschreibung der zu untersuchenden Phänomene, Prozesse oder Systeme in aller Regel zu Modellen, die sich aufgrund ihres Komplexitätsgrades einer analytischen Behandlung entziehen. […] Andererseits sind auch den Experimentiermöglichkeiten in vielen Fällen Grenzen gesetzt. Manchmal sind Experimente schlichtweg unmöglich. Vorgänge aus der Astrophysik wie beispielsweise der Lebenszyklus einer Galaxie dauern bei weitem zu lange, als dass ein noch so geduldiger Astronom sie beobachten könnte. Auch klimatische Phänomene wie der Treibhauseffekt sind äußerst unzugänglich: Man kann eben den weltweiten CO2N Ausstoß nicht so ohne weiteres für drei Monate verdoppeln oder halbieren, um die Auswirkungen zu messen, und Laborexperimente werfen hier natürlich die Frage der Skalierbarkeit auf. […] In anderen Fällen sind Experimente zwar machbar, aber aufgrund gefährlicher oder irreversibler Begleiterscheinungen unerwünscht – man denke nur etwa an Lawinenabgänge, Tests von Kernwaffen oder an Versuche am Menschen in der Medizin.»
>
> Quelle: Bungartz et al. (2002), [Simulierte Welten – die Zukunft im Rechner][2], S.38.

Anhand der obigen Beispiele wird ersichtlich, dass virtuelle Experimente in einer Simulationsumgebung reale Experimente ersetzen oder zumindest ergänzen können – vorausgesetzt, der Zusammenhang zwischen Realität und Simulation kann als gesichert gelten. Um diesen Zusammenhang herzustellen, müssen die zugrundeliegenden Theorien in numerische Modelle überführt, also in allen Aspekten konkret spezifiziert werden – so gesehen fördern Simulationen insbesondere auch die Theorie- bzw. Modellbildung.

Simulationen können theoretische Analyse und reales Experiment nicht ersetzen, aber ergänzen und besser verknüpfen. Die virtuelle Umsetzung beider Aspekte eröffnet neue Möglichkeiten, so etwa in Bezug auf die Genauigkeit der Theorien/Modelle, die Menge und Qualität der berücksichtigten Daten und die Präzision der abgeleiteten Vorhersagen. In besonderem Masse gilt das für die Erforschung von Systemen, die durch analytische Herangehensweisen nur ungenügend abgebildet werden können, unter anderem aufgrund komplexer Interaktionen, stochastischer Komponenten oder extremer Datenmengen. Aus diesen Gründen leisten Simulationen in der modernen Wissenschaft einen wesentlichen Beitrag dazu, dass sich aus der Wechselwirkung von Theorie und Evidenz (beides ggf. virtuell simuliert) immer bessere Erklärungen für verschiedenste Phänomene und Prozesse ergeben.

## Simulationen entwickeln und auswerten
Jede Simulation durchläuft drei Phasen:

- [Modellieren](?page=1-model/)
- [Simulieren](?page=2-simulate/)
- [Validieren](?page=3-validate/)

[1]: https://de.wikipedia.org/wiki/Ockhams_Rasiermesser
[2]: http://docplayer.org/72490441-Hans-joachim-bungartz-hans-juergen-herrmann-barbara-irmgard-wohlmuth-simulierte-welten-die-zukunft-im-rechner.html

[^1]: Quelle: [oinf.ch](https://oinf.ch/kurs/simulationen/), CC BY-NC-SA 4.0
